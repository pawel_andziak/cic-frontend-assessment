from .views import JSONResponse

class APIExceptionMiddleware(object):
    def process_exception(self, request, exception):
        if request.path.startswith('/api/'):
            return JSONResponse({'long_url': ['Unexpected error']}, status=400)