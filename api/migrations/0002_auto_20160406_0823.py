# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shortenedurl',
            name='short_url',
        ),
        migrations.AddField(
            model_name='shortenedurl',
            name='shortcut',
            field=models.CharField(max_length=32, default='aa'),
            preserve_default=False,
        ),
    ]
