from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from .serializers import ShortenedURLInputSerializer, ShortenedURLOutputSerializer
from django.shortcuts import get_object_or_404
from .models import ShortenedURL


class EmptyView(View):
    def get(self, request):
        return HttpResponse()

def redirect(request, shortcut):
    shortened_url = get_object_or_404(ShortenedURL, shortcut=shortcut)
    return HttpResponse("Would redirect to %s, if this wasn't a mockup" % shortened_url.long_url)

class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@csrf_exempt
def shorten(request):
    data = JSONParser().parse(request)
    input_serializer = ShortenedURLInputSerializer(data=data)
    if input_serializer.is_valid():
        input_serializer.save()
        output_serializer = ShortenedURLOutputSerializer(input_serializer.instance)
        return JSONResponse(output_serializer.data, status=200)
    return JSONResponse(input_serializer.errors, status=400)
