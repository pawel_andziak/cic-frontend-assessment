from django.contrib import admin
from .models import ShortenedURL

class ShortenedURLAdmin(admin.ModelAdmin):
    list_display = ('long_url','get_short_url',)

admin.site.register(ShortenedURL, ShortenedURLAdmin)
