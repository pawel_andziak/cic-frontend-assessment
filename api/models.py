from django.db import models
from frontend_assessment.settings import BASE_URL

class ShortenedURL(models.Model):
    long_url = models.URLField()
    shortcut = models.CharField(max_length=32)

    def get_short_url(self):
        return "%s%s" % (BASE_URL, self.shortcut)
