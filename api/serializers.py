from django.utils.crypto import get_random_string
from rest_framework import serializers

from .models import ShortenedURL

class ShortenedURLInputSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShortenedURL
        fields = ('long_url',)

    def create(self, validated_data):
        shortened_url = ShortenedURL(
            long_url = validated_data['long_url'],
            shortcut = get_random_string(8)
        )
        shortened_url.save()
        return shortened_url

class ShortenedURLOutputSerializer(serializers.ModelSerializer):
    short_url = serializers.CharField(source='get_short_url')

    class Meta:
        model = ShortenedURL
        fields = ('short_url', 'long_url')